import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Product } from "../app/product";

@Injectable({
  providedIn: 'root'
})

export class ProductinventoryService {

  Url = "http://localhost:59486/api/ProductInventory";
  constructor(private http: HttpClient) { }

  InsertProduct(product) {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post<Product[]>(this.Url, product, httpOptions)
  }

  GetProductRecord(): Observable<Product[]> {
    return this.http.get<Product[]>(this.Url)
  }

  DeleteProduct(id: string): Observable<string> {
    return this.http.delete<string>(this.Url + '/' + id);
  }

  GetProductById(id: string) {
    return this.http.get<Product>(this.Url + '/' + id);
  }

  UpdatProduct(product: Product) {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.http.post<Product[]>(this.Url + '/UpdateProduct/', product, httpOptions)
  }
}

