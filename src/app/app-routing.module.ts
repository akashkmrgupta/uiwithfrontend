import { NgModule } from '@angular/core';  
import { Routes, RouterModule } from '@angular/router';  
import { ProductComponent } from "./product/product.component";  
import { AddproductComponent } from "./addproduct/addproduct.component"; 
import { ViewproductComponent } from './viewproduct/viewproduct.component';

const routes: Routes = [  
   {path:"",component:ProductComponent},  
   {path:"addproduct",component:AddproductComponent},  
   {path:"addproduct/:id",component:AddproductComponent},
   {path:"viewproduct", component:ViewproductComponent}  
  ];

@NgModule({  
  imports: [RouterModule.forRoot(routes)],  
  exports: [RouterModule]  
})  
export class AppRoutingModule { }  