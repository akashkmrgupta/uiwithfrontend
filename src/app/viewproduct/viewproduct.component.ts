import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.css']
})
export class ViewproductComponent implements OnInit {
public productData:any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
    this.productData=this.router.getCurrentNavigation().extras.state;
   }

  ngOnInit(): void {
  }

}
